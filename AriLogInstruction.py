from baseInstruction import instruction, same_type_or_throw, type_any

from argument import ArgType
from util import BAD_TYPE, dp, Err, BAD_VAL

class ArithmeticInstr(instruction):
    """Base class for arithmetic instructions"""
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.Int, ArgType.Float], 2
        )
        same_type_or_throw(
            self.args[2].type, [ArgType.Var, ArgType.Int, ArgType.Float], 3
        )

    def get_lr(self, context):
        """returns 2nd adn 3rd arguments

        check if 2nd and 3rd arguments are of same type, if so, returns 2nd, 3rd argument and their type.
        """
        # if operator is String, use string, if it's uninitialized use empty
        # string, report error otherwise
        l_type = context.get_type(self.args[1])
        r_type = context.get_type(self.args[2])
        if l_type != r_type:
            raise Err(err_msg="Incompatible types {} and {} in {}"
                      .format(l_type, r_type, type(self).__name__),
                      exit_code=BAD_TYPE)

        left = context.get_value(self.args[1], [ArgType.Int, ArgType.Float])
        right = context.get_value(self.args[2], [ArgType.Int, ArgType.Float])

        return left, right, l_type


class ADD(ArithmeticInstr):

    def exec(self, context):
        left, right, type = self.get_lr(context)

        context.set(self.args[0], left + right, type)



class SUB(ArithmeticInstr):

    def exec(self, context):
        left, right, type = self.get_lr(context)

        context.set(self.args[0], left - right, type)



class MUL(ArithmeticInstr):

    def exec(self, context):
        left, right, type = self.get_lr(context)

        context.set(self.args[0], left * right, type)


class IDIV(ArithmeticInstr):

    def exec(self, context):
        left, right, type = self.get_lr(context)

        if type != ArgType.Int:
            raise Err(err_msg="Invalid type {} in IDIV (must be int)"
                      .format(type), exit_code=BAD_TYPE)

        if right == 0:
            raise Err(BAD_VAL, "Error: division by zero")

        context.set(self.args[0], left // right, ArgType.Int)

class DIV(ArithmeticInstr):

    def exec(self, context):
        left, right, type = self.get_lr(context)

        if type != ArgType.Int:
            raise Err(err_msg="Incompatible type {} in DIV (must be a float)"
                      .format(type),
                      exit_code=BAD_TYPE)

        if right == 0:
            raise Err(BAD_VAL, "Error: division by zero")

        context.set(self.args[0], left / right, ArgType.Float)



class CmpInstruction(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(self.args[1].type, type_any, 2)
        same_type_or_throw(self.args[2].type, type_any, 3)


    def get_lr(self, context, allow_nil):
        # if operator is String, use string, if it's uninitialized use empty
        # string, report error otherwise

        ltype = context.get_type(self.args[1])
        rtype = context.get_type(self.args[2])

        if ltype != rtype or ltype == ArgType.Nil:
            if allow_nil:
                if ltype != ArgType.Nil and rtype != ArgType.Nil:
                    raise Err(BAD_TYPE, "Incompatible types for comparision {} and {}".format(rtype, ltype))
            else:
                raise Err(BAD_TYPE, "Incompatible types for comparision {} and {}".format(rtype, ltype))

        left = context.get_value(self.args[1], [ArgType.Any])

        right = context.get_value(self.args[2], [ArgType.Any])

        return left, right


class LT(CmpInstruction):

    def exec(self, context):
        left, right = self.get_lr(context, allow_nil=False)

        context.set(self.args[0], left < right, ArgType.Bool)




class GT(CmpInstruction):

    def exec(self, context):
        left, right = self.get_lr(context, allow_nil=False)

        context.set(self.args[0], left > right, ArgType.Bool)



class EQ(CmpInstruction):
    def exec(self, context):
        left, right = self.get_lr(context, allow_nil=True)

        context.set(self.args[0], left == right, ArgType.Bool)


class LogicInstruction(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.Bool], 2
        )
        same_type_or_throw(
            self.args[2].type, [ArgType.Var, ArgType.Bool], 3
        )

    def get_lr(self, context):
        # if operator is String, use string, if it's uninitialized use empty
        # string, report error otherwise
        left = context.get_value(self.args[1], [ArgType.Bool])
        right = context.get_value(self.args[2], [ArgType.Bool])

        return left, right

class AND(LogicInstruction):

    def exec(self, context):
        left, right = self.get_lr(context)

        context.set(self.args[0], left and right, ArgType.Bool)



class OR(LogicInstruction):

    def exec(self, context):
        left, right = self.get_lr(context)

        context.set(self.args[0], left or right, ArgType.Bool)



class NOT(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.Bool], 2
        )

    def exec(self, context):
        not_notted = context.get_value(self.args[1], [ArgType.Bool])
        notted = not not_notted

        context.set(self.args[0], notted, ArgType.Bool)
