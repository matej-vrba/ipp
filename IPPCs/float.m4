divert(-1)
define(math,`
WRITE string@$1\032$2\032$3\032->\032
$1 GF@a float@$2 float@$3
WRITE GF@a
write string@\010
')
divert(0)dnl
.IPPcode23
DEFVAR GF@a

math(ADD, 10, 10)
math(ADD, 10.2, 10)
math(ADD, 0.2, 0)

math(SUB, 10, 10)
math(SUB, 10.2, 10)
math(SUB, 0.2, 0)
math(SUB, 0, 0.2)

math(MUL, 10, 10)
math(MUL, 10.2, 10)
math(MUL, 0.2, 0)
math(MUL, 10, 0.2)

math(DIV, 10, 10)
math(DIV, 10.2, 10)
math(DIV, 0, 0.2)
math(DIV, 10, 0.2)

math(LT, 10, 10)
math(LT, 10.2, 10)
math(LT, 0, 0.2)
math(LT, 10, 0.2)
math(LT, -10, 0.2)

math(GT, 10, 10)
math(GT, 10.2, 10)
math(GT, 0, 0.2)
math(GT, 10, 0.2)
math(GT, -10, 0.2)

math(EQ, 10, 10)
math(EQ, 10.2, 10)
math(EQ, 0, 0.2)
math(EQ, 10, 0.2)
math(EQ, -10, 0.2)
