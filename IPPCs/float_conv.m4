divert(-1)
define(i2f,`
write string@int2float\032$1\032->\032
int2float GF@a int@$1
write GF@a
write string@\010
')
define(f2i,`
write string@float2int\032$1\032->\032
float2int GF@a float@$1
write GF@a
write string@\010
')
define(f2is,`
write string@float2ints\032$1\032->\032
pushs float@$1
float2ints
pops GF@a
write GF@a
write string@\010
')
define(i2fs,`
write string@int2floats\032$1\032->\032
pushs int@$1
int2floats
pops GF@a
write GF@a
write string@\010
')
divert(0)dnl
.IPPcode23
DEFVAR GF@a


i2f(10)
i2f(0)
i2f(-10)
write string@\010

f2i(10.0)
f2i(0.0)
f2i(-10.0)
f2i(10.1)
f2i(10.4)
f2i(10.5)
f2i(10.6)
write string@\010

i2fs(10)
i2fs(0)
i2fs(-10)
write string@\010

f2is(10.0)
f2is(0.0)
f2is(-10.0)
f2is(10.1)
f2is(10.4)
f2is(10.5)
f2is(10.6)

f2i(0x1.20000000p+0)
f2is(0x1.20000000p+0)
write float@0x1.20000000p+0
write string@\010enter\032float:\032
read GF@a float
write GF@a
write string@\010
