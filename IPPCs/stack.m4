divert(-1)
define(ari, `cmp($1, int@$2, int@$3)')
define(log, `cmp($1, bool@$2, bool@$3)')
define(cmp, `
CLEARS
PUSHS $2
PUSHS $3
$1
pops GF@a
write string@$1\032$2\032$3\032->\032
write GF@a
write string@\010
')
define(conv, `
CLEARS
PUSHS int@$2
$1
pops GF@a
write string@$1\032$2\032->\032
write string@"
write GF@a
write string@"
write string@\010
')
define(jmp, `
PUSHS $2
PUSHS $3
$1 jmp$4
write string@$1\032$2\032$3\032did\032not\032jump\010
JUMP end$4
LABEL jmp$4
write string@$1\032$2\032$3\032jumped\010
LABEL end$4
')
#define(ari, `')
divert(0)
.IPPcode23
defvar GF@a
ari(ADDS, 10, 20)
ari(SUBS, 10, 20)
ari(MULS, 10, 20)
ari(IDIVS, 10, 20)
ari(IDIVS, 20, 10)
ari(IDIVS, 0, 10)

ari(LTS, 10, 20)
ari(GTS, 10, 20)
ari(EQS, 10, 20)

ari(LTS, 20, 10)
ari(GTS, 20, 10)
ari(EQS, 20, 10)

ari(LTS, 10, 10)
ari(GTS, 10, 10)
ari(EQS, 10, 10)


cmp(LTS, bool@true, bool@false)
cmp(GTS, bool@true, bool@false)
cmp(EQS, bool@true, bool@false)

cmp(LTS, bool@false, bool@true)
cmp(GTS, bool@false, bool@true)
cmp(EQS, bool@false, bool@true)

cmp(LTS, bool@true, bool@true)
cmp(GTS, bool@true, bool@true)
cmp(EQS, bool@true, bool@true)


cmp(LTS, string@a, string@b)
cmp(GTS, string@a, string@b)
cmp(EQS, string@a, string@b)

cmp(LTS, string@b, string@a)
cmp(GTS, string@b, string@a)
cmp(EQS, string@b, string@a)

cmp(LTS, string@a, string@a)
cmp(GTS, string@a, string@a)
cmp(EQS, string@a, string@a)

log(ORS, false, false)
log(ORS, false, true)
log(ORS, true, false)
log(ORS, true, true)

log(ANDS, false, false)
log(ANDS, false, true)
log(ANDS, true, false)
log(ANDS, true, true)

WRITE string@note\032to\032self:\032prvy\032argument\032je\032ignorovan\032:)\010
log(NOTS, true, false)
log(NOTS, false, true)

conv(INT2CHARS, 32)
conv(INT2CHARS, 65)
conv(INT2CHARS, 128829)
conv(INT2CHARS, 4281)

cmp(STRI2INTS, string@aAbB, int@0)
cmp(STRI2INTS, string@aAbB, int@1)
cmp(STRI2INTS, string@aAbB, int@2)
cmp(STRI2INTS, string@aAbB, int@3)

jmp(JUMPIFEQS, int@10, int@20, 1)
jmp(JUMPIFNEQS, int@10, int@20, 11)

jmp(JUMPIFEQS, int@10, int@10, 2)
jmp(JUMPIFNEQS, int@10, int@10, 12)

jmp(JUMPIFEQS, bool@true, bool@true, 3)
jmp(JUMPIFNEQS, bool@true, bool@true, 13)

jmp(JUMPIFEQS, bool@false, bool@true, 4)
jmp(JUMPIFNEQS, bool@false, bool@true, 14)
