divert(-1)
define(math,`
WRITE string@$1\032$2\032$3\032->\032
pushs float@$2
pushs float@$3
$1
pops GF@a
WRITE GF@a
write string@\010
')
divert(0)dnl
.IPPcode23
DEFVAR GF@a

math(ADDS, 10, 10)
math(ADDS, 10.2, 10)
math(ADDS, 0.2, 0)

math(SUBS, 10, 10)
math(SUBS, 10.2, 10)
math(SUBS, 0.2, 0)
math(SUBS, 0, 0.2)

math(MULS, 10, 10)
math(MULS, 10.2, 10)
math(MULS, 0.2, 0)
math(MULS, 10, 0.2)

math(DIVS, 10, 10)
math(DIVS, 10.2, 10)
math(DIVS, 0, 0.2)
math(DIVS, 10, 0.2)

math(LTS, 10, 10)
math(LTS, 10.2, 10)
math(LTS, 0, 0.2)
math(LTS, 10, 0.2)
math(LTS, -10, 0.2)

math(GTS, 10, 10)
math(GTS, 10.2, 10)
math(GTS, 0, 0.2)
math(GTS, 10, 0.2)
math(GTS, -10, 0.2)

math(EQS, 10, 10)
math(EQS, 10.2, 10)
math(EQS, 0, 0.2)
math(EQS, 10, 0.2)
math(EQS, -10, 0.2)
