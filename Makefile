##
# IPP Project 1 and 2
#
# Authors:
# Matěj Vrba <xvrbam03@stud.fit.vutbr.cz>
#

ARCHIVE = "xvrbam03"
#list of files to add to archive (source code is aleready included)
AR_ADD = *.py readme2.pdf rozsireni
TEXIFILES = $(wildcard *.texi)
PDF_DOCS = $(TEXIFILES:.texi=.pdf)

UML_SRC = $(wildcard *.pum)
UMLS = $(UML_SRC:.pum=.png)

M4_SRC = $(wildcard ./IPPCs/*.m4)
IPPCS = $(M4_SRC:.m4=.ippc)

IPPCS += $(wildcard ./IPPCs/*.ippc)
XMLS = $(IPPCS:.ippc=.xml)

TEXI2PDF = texi2pdf # program used to convert texi files to pdf
SHELL = /bin/bash #just to be sure PHP = php8.2


.PHONY: all clean run docs $(TESTS)

all: $(TARGET) $(XMLS) docs

test: $(TESTS)

$(TESTS):
	$(shell $(PHP) parse.php  < $@ > $(@:.src=.my_out) 2>/dev/null)
	-@test $(.SHELLSTATUS) -eq $(shell cat $(@:.src=.rc))  || echo "Test " $@ "failed, wrong exit code"
	@diff \
<(xmllint --c14n $(@:.src=.my_out) 2> /dev/null  | tr -d " " || echo"") \
<(xmllint --c14n $(@:.src=.out) 2> /dev/null  | tr -d " " || echo"")
	$(eval test_succ=$(shell echo $$(($(test_succ)+1))))
	@printf '\rTest %d/%d' $(test_succ) $(test_total)

clean:
	@echo Removing texinfo files
	@- $(RM) *.aux *.cp *.cps *.log *.toc
	@echo Removing archive
	@- $(RM) $(ARCHIVE).tar
	@echo "Removing xml files (ipp code intermediates)"
	@- $(RM) $(XMLS)

pack: $(ARCHIVE).tgz

$(ARCHIVE).tgz: $(SRC) $(AR_ADD)
# check for TOODs and ignore this search
	tar -czf $@ $^

%.xml: %.ippc
	php parse.php < $^ > $@

%.ippc: %.m4
	m4 $^ > $@

docs: $(PDF_DOCS) $(UMLS)

%.png: %.pum
	plantuml $^

%.pdf: %.texi $(UMLS)
	$(TEXI2PDF) $< -o $@
