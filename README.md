# IPP - Principles of Programming Languages
[IPP project](https://www.fit.vut.cz/study/course/IPP/.en) - Parser and interpret of IPPcode.
IPPCode is Three-address code, where every line is either empty, contains comment or instruction, instruction starts with opcode (e.g. `ADD` and is followed by it's arguments (up to three)).
Arguments are in format `ID@VAL/NAME`, where `ID` identifies type of contant (int, string, float, bool), or variable (Temporary frame - `TF`, Local frame - `LF` or Global frame - `GF`).
If argument is contant, `VAL/NAME` is it's value, otherwise if argument is variable, `VAL/NAME` contains name of variable.
Name can contains almost any symbol (except for whitespace, `#` or `@`).
String have all characters with ascii value 0-32 replaced with `\XXX` sequence, where `XXX` is decimal value of that character.

# Project structure

Project is separated into parser (`parse.php`) and interpret (`*.py`).
Parser expects IPPCode as input on `stdin` and outputs to `stdout` parsed input in XML, on error prints to `stderr`, exits with non-zero exti code.
Interpret ("main" is in `interpret.py`) expects parsed IPPCode (in xml) on input (either stdin or `--source=FILE` argument) and stdin of parsed program (either stdin or `--input=FILE` argument) at least one of the two must be read from file, other can be from stdin.
Interpret outputs the output of interpreted program to stdout, on error prints to stderr and exits with non-zero code.

For more info see `readme1` and `readme2` (can be built to pdf using `make docs`).
