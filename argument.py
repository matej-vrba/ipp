#!/usr/bin/env python3
from enum import Enum
from util import todo, Err, ERR_INTERNAL, BAD_TYPE, MALFORMED_XML
import re


def parse_arg(xml_arg):
    """
    Parse xml input as argument, returns coresponding variable or constant.

    expect xml to contain key "type", valid types are defined in
    `arg_type_parse_table`
    """
    type = None
    for (key, val) in xml_arg.items():
        if key == "type":
            type = arg_type_parse_table.get(val)
    if type is None:
        raise Err(err_msg="Unknown type of {} - {}".format(xml_arg.tag, xml_arg.text),
                  exit_code=MALFORMED_XML)

    pattern = re.compile("^arg[1-3]$")
    if not pattern.match(xml_arg.tag):
        raise Err(exit_code=MALFORMED_XML, err_msg="Invlaid tag name {}".format(xml_arg.tag))
    if type == ArgType.Var:
        return variable(name=xml_arg.text.strip())

    try:
        return constant(type=type, val=xml_arg.text)
    except ValueError as e:
        raise Err(err_msg=e.args[0], exit_code=BAD_TYPE)


class argument:
    """
    Class representing one instruction's argument.

    Holds value and type of said argument
    """

    def set(self, type, val):
        """Set type and value."""
        self.type = type
        self.val = val


class variable(argument):
    """
    Class representing variable.

    holds type, value and name of the variable.
    """

    def __init__(self, name):
        """Create new variable of type uninitialized and value None."""
        self.set(ArgType.Var, None)
        self.name = name

    def get(self):
        """Get type and value of variable."""
        return self.type, self.val


class constant(argument):
    """Class representing value constant."""

    def __init__(self, type, val):
        """
        Initialize new constant.
        If `val` is None, value is set to:
        - string it's empty string
        - int and float 0
        - type "type" constructor throws NotImplemented
        - bool - false

        for Nil `val` is ignored and value set to None
        """
        if val is not None:
            val = val.strip()

        if type == ArgType.Label and val is None:
            raise Err(err_msg="Missing label name", exit_code=ERR_INTERNAL)

        if type == ArgType.String or type == ArgType.Label:
            if val is None:
                self.set(type, "")
            else:
                self.set(type, val)

        elif type == ArgType.Int:
            if val is None:
                self.set(type, 0)
            try:
                self.set(type, int(val))
            except ValueError:
                raise ValueError("Cannot parse \"{}\" as base 10 integer"
                                 .format(val))

        elif type == ArgType.Float:
            if val is None:
                self.set(type, 0.0)
            try:
                if val.startswith("0x") or val.startswith("0X"):
                    self.set(type, float.fromhex(val))
                else:
                    self.set(type, float(val))
            except ValueError:
                raise ValueError("Cannot parse \"{}\" as float"
                                 .format(val))

        elif type == ArgType.Type:
            if val is None:
                raise NotImplementedError
            self.set(type, val)
        elif type == ArgType.Nil:
            self.set(type, None)
        elif type == ArgType.Bool:
            if val == "true":
                self.set(type, True)
            elif val == "false":
                self.set(type, False)
            else:
                raise ValueError("Invalid boolean value \"{}\", expected true or false"
                                 .format(val))
        else:
            todo()

        self.name = None


# TODO refacto
class ArgType(Enum):
    """
    Enum of possible types.

    Types other from IPPCode specification include:
    - Var - Any variable
    - Var - Variable before it's first assigned
    - Type - used by second argument of READ
    - Any - Anything
    """

    Var = "var"
    Label = "label"
    String = "string"
    Bool = "bool"
    Int = "int"
    Nil = "nil"
    Type = "type"
    Any = "Any"
    Float = "float"


arg_type_parse_table = {
    "var": ArgType.Var,
    "label": ArgType.Label,
    "string": ArgType.String,
    "bool": ArgType.Bool,
    "int": ArgType.Int,
    "nil": ArgType.Nil,
    "type": ArgType.Type,
    "float": ArgType.Float,
}
