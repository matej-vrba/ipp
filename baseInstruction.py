import abc
from argument import ArgType, parse_arg
import inspect
from util import Err, ERR_INTERNAL, BAD_TYPE, BAD_XML_CONTENT

type_any = [
    ArgType.Var,
    ArgType.String,
    ArgType.Bool,
    ArgType.Int,
    ArgType.Nil,
    ArgType.Float,
]


def same_type_or_throw(unknown, expected, num):
    """
    checks if expected == unknown.

    If check fails throws `Err` with name, instruction number (`num`),
    expected and received type.

    If `similar` is true, aditional checks are performed (for example
    if `similar` is true and ArgType is variable, unknows is accepted if it's
    String, int or variable)
    """
    for ex in expected:
        if unknown == ex:
            return
    # https://stackoverflow.com/a/17065634 anwser by "brice" on 2013-06-12
    stack = inspect.stack()
    the_class = stack[1][0].f_locals["self"].__class__.__name__
    raise Err(err_msg="Invalid argument {} of {}, expected one of {}, but got {}".format(
            num, the_class, expected, unknown
    ), exit_code=BAD_TYPE)

class instruction(abc.ABC):
    """Base class for all instructions."""

    @abc.abstractmethod
    def check_arg_types(self):
        """Implemented by every instruction, checks that arguments are of correct type"""

    def __init__(self, instr):
        """
        Constructo for base instruction.

        Checks argument count and loads arguments
        """

        if __debug__:
            if self.__class__.__name__ == "instruction":
                raise Err(ERR_INTERNAL, "attempted to construct base instruction")
        # check number of arguments
        if instr.__len__() != self.arg_n:
            raise Err(BAD_XML_CONTENT, "{}: Invalid argument count"
                      .format(self.__class__.__name__))

        # deduce instruction name from class name
        self.name = self.__class__.__name__

        # get arguments from input xml
        self.args = [None, None, None]
        for input_arg in instr:
            self.args[int(input_arg.tag[3]) - 1] = parse_arg(input_arg)
        self.args = [x for x in self.args if x is not None]
        self.check_arg_types()
#        self.print()

    def print(self):
        """Print instruction name and arguments."""
        print("instr {}: ".format(self.name), end=" ")
        for line, arg in enumerate(self.args):
            print("arg{}".format(line), arg.name, arg.type, end=" ")
        print("")

    def exec(self, context):
        print("{} is valid, but not implemented".format(self.name))
