from baseInstruction import instruction, same_type_or_throw, type_any

from argument import ArgType
from util import dp, STRING_ERR, Err

class INT2CHAR(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.Int], 2
        )

    def exec(self, context):
        to_convert = context.get_value(self.args[1], [ArgType.Int])

        try:
            converted = chr(to_convert)
        except ValueError:
            raise Err(STRING_ERR, "Failed to convert {} to unicode".format(to_convert))

        context.set(self.args[0], converted, ArgType.String)



class STRI2INT(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.String], 2
        )
        same_type_or_throw(
            self.args[2].type, [ArgType.Var, ArgType.Int], 2
        )

    def exec(self, context):
        src_str = context.get_value(self.args[1], [ArgType.String])
        pos = context.get_value(self.args[2], [ArgType.Int])

        if pos >= len(src_str):
            raise Err(STRING_ERR, "STRI2INT: Index {} out of bounds of string \"{}\"".format(pos, src_str))
        converted = ord(src_str[pos])

        context.set(self.args[0], converted, ArgType.Int)

class INT2FLOAT(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.Int], 2
        )

    def exec(self, context):
        to_convert = context.get_value(self.args[1], [ArgType.Int])

        try:
            converted = float(to_convert)
        except ValueError:
            raise Err(STRING_ERR, "Failed to convert {} to float".format(to_convert))

        context.set(self.args[0], converted, ArgType.Float)

class FLOAT2INT(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.Float], 2
        )

    def exec(self, context):
        to_convert = context.get_value(self.args[1], [ArgType.Float])

        try:
            converted = int(to_convert)
        except ValueError:
            raise Err(STRING_ERR, "Failed to convert {} to int".format(to_convert))

        context.set(self.args[0], converted, ArgType.Int)
