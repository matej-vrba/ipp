from baseInstruction import instruction, same_type_or_throw, type_any
from argument import ArgType
from util import BAD_TYPE, BAD_SOURCE, Err, BAD_VAL
import sys



class CREATEFRAME(instruction):
    arg_n = 0

    def check_arg_types(self):
        {}  # python needs something here, this instruction has no arguments -> nothing to check

    def exec(self, context):
        context.create_frame()



class PUSHFRAME(instruction):
    arg_n = 0

    def check_arg_types(self):
        {}

    def exec(self, context):
        context.push_frame()



class POPFRAME(instruction):
    arg_n = 0

    def check_arg_types(self):
        {}

    def exec(self, context):
        context.pop_frame()



class DEFVAR(instruction):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)

    def exec(self, context):
        context.define_variable(self.args[0].name)



class MOVE(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(self.args[1].type, type_any, 2,)

    def exec(self, context):
        right = context.get_value(self.args[1], [ArgType.Any])
        context.set(self.args[0], right, context.get_type(self.args[1]))


class CALL(instruction):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Label], 1)

    def exec(self, context):
        if context.labels.get(self.args[0].val) is None:
            raise Err(BAD_SOURCE, "Label {} doesnt exits".format(self.args[0].val))
        context.call_stack_push(self.args[0].val)
        return context.labels[self.args[0].val]


class RETURN(instruction):
    arg_n = 0

    def check_arg_types(self):
        {}

    def exec(self, context):
        (next_instr, _) = context.call_stack_pop()
        return next_instr + 1


class LABEL(instruction):
    arg_n = 1

    def check_arg_types(self):
        assert self.args[0].type == ArgType.Label

    def exec(self, context):
        return


class JUMP(instruction):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Label], 1)

    def exec(self, context):
        target = context.labels.get(self.args[0].val)
        if target is None:
            raise Err("Invalid jump target \"{}\"".format(self.args[0].val))
        return context.labels[self.args[0].val]


class JUMPIFEQ(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Label], 1)
        if (
            self.args[1].type != self.args[2].type
            and self.args[1].type != ArgType.Var
            and self.args[2].type != ArgType.Var
        ):
            raise Err(err_msg="Invalid argument number 2 of JUMPIFEQ",
                      exit_code=BAD_TYPE)

    def exec(self, context):
        left = context.get_value(self.args[1], [ArgType.Any])
        right = context.get_value(self.args[2], [ArgType.Any])
        ltype = context.get_type(self.args[1])
        rtype = context.get_type(self.args[2])

        if ltype != rtype:
            raise Err(BAD_TYPE, "Cannot compare incompatible types {} and {}"
                      .format(ltype, rtype))

        if left == right:
           return self.args[0].val




class JUMPIFNEQ(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Label], 1)
        if (
            self.args[1].type != self.args[2].type
            and self.args[1].type != ArgType.Var
            and self.args[2].type != ArgType.Var
        ):
            raise Err(err_msg="Invalid argument number 2 of JUMPIFEQ",
                               exit_code=BAD_TYPE)

    def exec(self, context):
        left = context.get_value(self.args[1], [ArgType.Any])
        right = context.get_value(self.args[2], [ArgType.Any])
        ltype = context.get_type(self.args[1])
        rtype = context.get_type(self.args[2])

        if ltype != rtype:
            raise Err(BAD_TYPE, "Cannot compare incompatible types {} and {}"
                      .format(ltype, rtype))

        if left != right:
            return self.args[0].val


class EXIT(instruction):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(
            self.args[0].type, [ArgType.Var, ArgType.Int], 1
        )
    def exec(self, context):
        code = context.get_value(self.args[0], [ArgType.Int])
        if code >= 0 and code <= 49:
            sys.exit(code)

        raise Err(BAD_VAL,
                  "Invalid exit code {}, valid interval is <0, 49>"
                  .format(code))
