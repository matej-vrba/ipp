from util import dp

from flowControl import *
from stackInstructions import *
from AriLogInstruction import *
from conversionInstructions import *
from stringInstructions import *
from ioInstructions import *


# dictonary mapping instruction name to it's coresponding class
instr_map = {
    "CREATEFRAME": CREATEFRAME,
    "PUSHFRAME": PUSHFRAME,
    "POPFRAME": POPFRAME,
    "DEFVAR": DEFVAR,
    "MOVE": MOVE,
    "CALL": CALL,
    "RETURN": RETURN,
    "PUSHS": PUSHS,
    "POPS": POPS,
    "ADD": ADD,
    "SUB": SUB,
    "MUL": MUL,
    "IDIV": IDIV,
    "DIV": DIV,
    "LT": LT,
    "GT": GT,
    "EQ": EQ,
    "AND": AND,
    "OR": OR,
    "NOT": NOT,
    "INT2CHAR": INT2CHAR,
    "STRI2INT": STRI2INT,
    "READ": READ,
    "WRITE": WRITE,
    "CONCAT": CONCAT,
    "STRLEN": STRLEN,
    "GETCHAR": GETCHAR,
    "SETCHAR": SETCHAR,
    "TYPE": TYPE,
    "LABEL": LABEL,
    "JUMP": JUMP,
    "JUMPIFEQ": JUMPIFEQ,
    "JUMPIFNEQ": JUMPIFNEQ,
    "EXIT": EXIT,
    "DPRINT": DPRINT,
    "BREAK": BREAK,
    "ADDS": ADDS,
    "CLEARS": CLEARS,
    "SUBS": SUBS,
    "MULS": MULS,
    "IDIVS": IDIVS,
    "DIVS": DIVS,
    "LTS": LTS,
    "GTS": GTS,
    "EQS": EQS,
    "ORS": ORS,
    "ANDS": ANDS,
    "NOTS": NOTS,
    "INT2CHARS": INT2CHARS,
    "STRI2INTS": STRI2INTS,
    "JUMPIFEQS": JUMPIFEQS,
    "JUMPIFNEQS": JUMPIFNEQS,
    "INT2FLOAT": INT2FLOAT,
    "FLOAT2INT": FLOAT2INT,
    "INT2FLOATS": INT2FLOATS,
    "FLOAT2INTS": FLOAT2INTS,
}
