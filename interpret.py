#!/usr/bin/env python3
import argparse
import sys
import xml.dom.minidom
import xml.etree.ElementTree
from util import eprint
from argument import variable
from instructions import instr_map, ArgType
from util import dp, todo, Err, BAD_TYPE, NO_VAL, BAD_XML_CONTENT, MALFORMED_XML, BAD_SOURCE, NO_FRAME



class context:
    """
    Contains interpretation context (currently defined variables, their type
    and value, call and data stack,...).

    context is passed to every instruction during it's execution.
    """

    def __init__(self, stdin):
        # instruction pointer - index which instruction to execute next
        self.instr_pointer = 0
        self.labels = {}
        # LF, TF and GF
        self.frame_stack = []
        self.temp_frame = None
        self.global_frame = {}
        # used by call
        self.call_stack = []
        # used by pushs and pops
        self.data_stack = []
        self.stdin = stdin


    def define_variable(self, name, val=None):
        """Define variable

        Expects name to contain frame (e.g. GF@variable not just variable)
        Optionaly initial value can be included.
        If variable is aleready defined, exception is thrown.
        """
        if self.__get_frame(name).get(name[3:]) is None:
            self.__get_frame(name)[name[3:]] = variable(val)
        else:
            raise Err(BAD_SOURCE, "{} aleready defined".format(name))

    def push_frame(self):
        """Pushes temp frame onto frame stack

        Throws exception of temp frame doesn't exist
        """
        if self.temp_frame is None:
            raise Err(err_msg="Cannot push frame, TF does not exist", exit_code=NO_FRAME)
        self.frame_stack.append(self.temp_frame)
        self.temp_frame = None

    def pop_frame(self):
        """Pops frame from stack of local frames to the temp frame

        throws exception if frame stack is empty
        """
        if len(self.frame_stack) == 0:
            raise Err(NO_FRAME, "Local frame does not exist")
        self.temp_frame = self.frame_stack.pop()

    def create_frame(self):
        self.temp_frame = {}

    def __get_frame(self, name):
        """Takes variable name as a argument, returns coresponding frame.

        Throws if coresponding frame doesn't exist.
        """
        if name.startswith("GF@"):
            return self.global_frame
        elif name.startswith("LF@"):
            if len(self.frame_stack) == 0:
                raise Err(NO_FRAME, "Local frame does not exist")
            return self.frame_stack[len(self.frame_stack) - 1]
        elif name.startswith("TF@"):
            if self.temp_frame is not None:
                return self.temp_frame
            raise Err(err_msg="Temp frame does not exist", exit_code=NO_FRAME)
        else:
            raise Err(err_msg="Unknown frame of {}".format(name), exit_code=BAD_SOURCE)

    def get(self, arg):
        """Takes variable name as a argument, returns variable"""
        if self.__get_frame(arg.name).get(arg.name[3:]) is None:
            raise Err(54, "{} not defined".format(arg.name))
        return self.__get_frame(arg.name)[arg.name[3:]]

    def set(self, arg, val, type):
        """Sets variable `arg` to value `val` and type `type`"""
        self.get(arg).val = val
        self.get(arg).type = type

    def get_value(self, arg, types):
        """Returns value of variable or constant.

        expects variable name or constant in `arg`, array of possible types in `types`

        if `arg` is variable and matches type in `types` it's value is returned.
        if not exception is thrown.

        if `arg` is constant, type is checked and either value is returned or exception is thrown.
        if `arg` is an uninitialized variable, default value is returned.

        """

        # Only variables have name (if name is None, arg is a constant)
        if arg.name is not None and\
           self.get(arg).type != ArgType.Var:
            # It is initialized variable, check type
            if self.get(arg).type not in types and\
               ArgType.Any not in types:
                # Incompatible types
                raise Err(err_msg="Incompatible types: {} vs {}"
                          .format(type, self.get(arg).type),
                          exit_code=BAD_TYPE)
            # argument is variable of compatible type
            return self.get(arg).val
        else:
            # arg is either constant or uninitialized variable
            # check if it's uninitialized variable, if so return "neutral"(in
            # most cases) value
            if arg.type == ArgType.Var:
                if ArgType.String in types:
                    return ""
                if ArgType.Int in types:
                    return 0
                if ArgType.Float in types:
                    return 0.0
                if ArgType.Bool in types:
                    return False
                if ArgType.Nil in types:
                    return None
                else:
                    return None#TOOD
            # arg is constant, check type
            elif arg.type in types or ArgType.Any in types:
                return arg.val
            else:
                raise Err(BAD_TYPE, "Incompatible types {} vs one of {}"
                          .format(arg.type, types))

    def get_type(self, arg):
        """return type of `arg`"""
        # Only variables have name
        if arg.name is None:
            # constants have type as member variable
            return arg.type
        else:
            # for variable we need to get it from current frame
            return self.get(arg).type

    def add_label(self, name, pos):
        """Register new label `name` at position `pos`

        `pos` is a instruction number.
        """
        if self.labels.get(name) is None:
            self.labels[name] = pos
        else:
            raise Err(err_msg="Label {} aleready exists (instruction {})"
                      .format(name, self.labels.get(name)), exit_code=BAD_SOURCE)

    def call_stack_push(self, note):
        """
        Push current instruction pointer onto call stack alongside with note.

        `note` is used in DPRINT to give some context to the call stack.
        """
        self.call_stack.append((self.instr_pointer, note))

    def call_stack_pop(self):
        if(len(self.call_stack) == 0):
            raise Err(err_msg="Cannot return, call stack is empty",
                      exit_code=NO_VAL)
        return self.call_stack.pop()

    def data_stack_push(self, var):
        self.data_stack.append(var)

    def data_stack_pop(self):
        if(len(self.data_stack) == 0):
            raise Err(err_msg="Cannot pop from empty data stack",
                      exit_code=NO_VAL)
        return self.data_stack.pop()

    def data_stack_clear(self):
        self.data_stack.clear()


class interpret:
    """
    Interpret class.

    Contains instructions (once loaded) and necessary context
    """

    def __init__(self, source, input):
        """
        Interpret constructor.

        Parameters
        ----------
        source: file from which to read xml source
        input: interpreted program's stdin
        """
        self.source = source
        self.context = context(input)
        self.instructions = {}

    def __load(self):
        """Load xml from self.source into array of instructions."""
        try:
            self.dom = xml.etree.ElementTree.parse(self.source)
        except FileNotFoundError:
            if self.source == sys.stdin:
                raise Err(err_msg="Failed to read from stdin", exit_code=11)
            else:
                raise Err(err_msg="Cannot open file {}"
                          .format(self.source), exit_code=11)
        except xml.etree.ElementTree.ParseError:
            raise Err(err_msg="Failed to parse source as XML",
                      exit_code=MALFORMED_XML)

        # check root tag
        if self.dom.getroot().tag != "program":
            raise Err(
                err_msg="Malformed source XML, expected root tag to be \"program\"",
                exit_code=BAD_XML_CONTENT)
        if self.dom.getroot().attrib.get('language') is None:
            raise Err(
                err_msg="Program tag is missing language attribute",
                exit_code=BAD_XML_CONTENT)
        if self.dom.getroot().attrib.get('language') != "IPPcode23":
            raise Err(
                err_msg="Program tags contains language attribute with invalid value, expected \"IPPcode23\"",
                exit_code=BAD_XML_CONTENT)

        for instr in self.dom.getroot():
            if instr.tag != "instruction":
                raise Err(exit_code=BAD_XML_CONTENT,
                          err_msg="Expected instruction tag in input XML, got {}"
                          .format(instr.tag))

            opcode = instr.attrib["opcode"]
            # read order from instruction
            try:
                order = int(instr.attrib["order"])
            except ValueError:
                raise Err(
                    err_msg="Invalid order: \"{}\"(failed to parse as base 10 number)"
                    .format(instr.attrib["order"]), exit_code=BAD_XML_CONTENT)
            # check if order is positive
            if order <= 0:
                raise Err(
                    err_msg="Invalid order: \"{}\"(cannot be zero or negative)"
                    .format(order), exit_code=BAD_XML_CONTENT)

            # check if opcode is valid
            if opcode.upper() in instr_map:
                # create new instance of instruction
                i = instr_map[opcode.upper().strip()](instr)
                # labels need special treatment
                if i.name == "LABEL":
                    self.__add_label(i)
                # check if instruction with same index(order) exists
                if self.instructions.get(order - 1) is not None:
                    raise Err(err_msg="Duplicite instruction order {}"
                              .format(order), exit_code=BAD_XML_CONTENT)
                # valid instruction yay
                self.instructions[order-1] = i

            else:
                raise Err(err_msg='Unrecognised instruction "{}"'
                          .format(opcode), exit_code=BAD_SOURCE);

        return

    def __add_label(self, instr):
        return self.context.add_label(instr.args[0].val, len(self.instructions))

    def run(self):
        self.__load()
        should_end = False
        instr_list = list(self.instructions)
        instr_list.sort()
        try:
            self.context.instr_pointer = instr_list[0]
        except (ValueError, IndexError):
            return

        while not should_end:
            next_instr = self.instructions[
                self.context.instr_pointer
            ].exec(self.context)

            # when jumping to a label, next_instr is name of the label (string)
            # when program is continuing with next instruction next_instr is None
            if type(next_instr) is str:
                self.context.instr_pointer = self.context.labels[next_instr]
            elif next_instr is None:
                try:
                    self.context.instr_pointer = instr_list[instr_list.index(self.context.instr_pointer) + 1]
                except (ValueError, IndexError):
                    should_end = True
            else:
                self.context.instr_pointer = next_instr

            # check if instruction pointer is past last instruction
            if self.context.instr_pointer > instr_list[-1]:
                should_end = True

#            print(self.context.instr_pointer + 1)


# parse arguments
parser = argparse.ArgumentParser(description="Interpret jazyka IPPCode23")
parser.add_argument("--source", help="file with source code XML")
parser.add_argument("--input", help="Intepreted program's STDIN")
args = parser.parse_args()


# at input or source must be specified
if args.input is None and args.source is None:
    eprint("Either source or input must be specified")
    eprint("use " + sys.argv[0] + " --help for usage information")
    raise Err(10)

try:
    if args.input is not None:
        interptet_stdin = open(args.input, 'r')
    else:
        interptet_stdin = sys.stdin

    if args.source is not None:
        interptet_source = open(args.source, 'r')
    else:
        interptet_source = sys.stdin
# if input is specified it should be used instead of stdin
except FileNotFoundError:
    f = args.input if args.input is not None else args.source
    eprint("Failed to open {}".format(f))
    exit(11)
except PermissionError:
    f = args.input if args.input is not None else args.source
    eprint("{}: Permission denied".format(f))
    exit(11)


interp = interpret(input=interptet_stdin, source=interptet_source)
try:
    interp.run()
except Err as e:
    eprint(e)
    exit(e.exit_code)
