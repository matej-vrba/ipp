from baseInstruction import instruction, same_type_or_throw, type_any
import re
from argument import ArgType
from util import BAD_TYPE, dp, eprint, Err, STRING_ERR
import sys

class READ(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(self.args[1].type, [ArgType.Type], 2)

    def exec(self, context):

        # try to read from stdin
        try:
            raw_in = context.stdin.readline()
        except EOFError:
            context.set(self.args[0], None, ArgType.Nil)
            return
        if not raw_in:  # EOF?
            context.set(self.args[0], None, ArgType.Nil)
            return

        # reading was successfull, check expected type
        if self.args[1].val == "int":
            try:
                context.set(self.args[0], int(raw_in), ArgType.Int)
            except ValueError:
                context.set(self.args[0], None, ArgType.Nil)
        elif self.args[1].val == "string":
            context.set(self.args[0], raw_in, ArgType.String)
        elif self.args[1].val == "float":
            try:
                if raw_in.startswith("0x") or raw_in.startswith("0X"):
                    context.set(self.args[0], float.fromhex(raw_in), ArgType.Float)
                else:
                    context.set(self.args[0], float(raw_in), ArgType.Float)
            except ValueError:
                context.set(self.args[0], None, ArgType.Nil)
        elif self.args[1].val == "bool":
            if raw_in.upper().strip() == "TRUE":
                context.set(self.args[0], True, ArgType.Bool)
            else:
                context.set(self.args[0], False, ArgType.Bool)
        else:
            raise Err(BAD_TYPE, "Invalid type, cannot read {}".format(self.args[1].val))




class CommonPrinter(instruction):
    """class implementinc functionality of DPRINT and WRITE"""
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, type_any, 1)

    def print(self, context, file):
        if context.get_type(self.args[0]) == ArgType.String:
            raw_str = context.get_value(self.args[0], [ArgType.Any])
            # parse string
            # look for '\xxx'
            c = re.search("\\\\...", raw_str)  # find first occurence of \xxx
            while c is not None:
                # parse xxx from \xxx as int
                try:
                    i = int(raw_str[c.start() + 1 : c.end()])
                except ValueError:
                    raise Err(err_msg="Invalid escape sequence \"{}\" in {}"
                              .format(raw_str[c.start() + 1: c.end()], raw_str), exit_code=STRING_ERR)
                # convert xxx to ascii char of same value and insert it into the string
                raw_str = raw_str[: c.start()] + chr(i) + raw_str[c.end():]
                c = re.search("\\\\...", raw_str)

            print(raw_str, end="", file=file)
        elif context.get_type(self.args[0]) == ArgType.Nil:
            {}
        elif context.get_type(self.args[0]) == ArgType.Bool:
            val = context.get_value(self.args[0], [ArgType.Any]);
            if val == True:
                print("true", end="", file=file)
            else:
                print("false", end="", file=file)

        else:
            print(context.get_value(self.args[0], [ArgType.Any]), end="", file=file)
        file.flush()

class WRITE(CommonPrinter):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, type_any, 1)

    def exec(self, context):
        self.print(context, sys.stdout)


class DPRINT(CommonPrinter):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, type_any, 1)

    def exec(self, context):
        self.print(context, sys.stderr)



class BREAK(instruction):
    arg_n = 0

    def check_arg_types(self):
        return

    def __print_frame(self, frame, prefix, msg):
        if frame is not None and len(frame) > 0:
            print(msg, end="", file=sys.stderr)
            for key in frame:
                val = frame[key]
                print("{}@{}:{}({}) ".format(prefix, key, val.val, val.type._name_), end="", file=sys.stderr)
            eprint("")

    def exec(self, context):
        # TODO add description of BREAK in documentation
        print("Call stack: ", end="", file=sys.stderr)
        for (instr, func) in context.call_stack:
            print("{}:{} ".format(func, instr), end="", file=sys.stderr)
        eprint("")
        eprint("IP: {}".format(context.instr_pointer))

        # print frames
        self.__print_frame(context.global_frame, "GF", "Global frame: ")

        self.__print_frame(context.temp_frame, "TF", "Temp Frame: ")
        if len(context.frame_stack) > 0:
            self.__print_frame(context.frame_stack[len(context.frame_stack) - 1],
                               "LF", "Local Frame: ")

        print("Data stack:", file=sys.stderr, end="")
        for (data, t) in context.data_stack:
            print(" {}({})".format(data, t._name_), file=sys.stderr, end="")
        print("", file=sys.stderr)
