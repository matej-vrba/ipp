<?php
ini_set('display_errors', 'stderr');


// Enum of possible argument types using bitfield
class ArgType
{
    const Int = 1 << 0;
    const Bool = 1 << 1;
    const String = 1 << 2;
    const Nil = 1 << 3;
    const Var = 1 << 4;
    const Type = 1 << 5;
    const Label = 1 << 6;
    const Float = 1 << 0;
    const Any = 0xff;
    const Symbol = ArgType::Int |
        ArgType::Bool |
        ArgType::String |
        ArgType::Nil |
        ArgType::Float |
        ArgType::Var;
};

// array mapping argument type to it's bitfield value
$arg_types = array(
    "int"    => ArgType::Int,
    "bool"   => ArgType::Bool,
    "string" => ArgType::String,
    "nil"    => ArgType::Nil,
    "var"    => ArgType::Var,
    "type"   => ArgType::Type,
    "label"  => ArgType::Label,
    "any"    => ArgType::Any,
    "symbol" => ArgType::Symbol,
    "float" => ArgType::Float,
);


// array mapping instruction to expected arguments
$opcodes = array(
    "CREATEFRAME" => array(),
    "PUSHFRAME" => array(),
    "POPFRAME" => array(),
    "DEFVAR" => array(ArgType::Any),
    "MOVE" => array(ArgType::Var, ArgType::Symbol),
    "CALL" => array(ArgType::Label),
    "RETURN" => array(),
    "PUSHS" => array(ArgType::Symbol),
    "POPS" => array(ArgType::Var),
    "ADD" => array(ArgType::Var, ArgType::Int | ArgType::Var, ArgType::Int | ArgType::Var),
    "SUB" => array(ArgType::Var, ArgType::Int | ArgType::Var, ArgType::Int | ArgType::Var),
    "MUL" => array(ArgType::Var, ArgType::Int | ArgType::Var, ArgType::Int | ArgType::Var),
    "IDIV" => array(ArgType::Var, ArgType::Int | ArgType::Var, ArgType::Int | ArgType::Var),
    "DIV" => array(ArgType::Var, ArgType::Int | ArgType::Var, ArgType::Int | ArgType::Var),
    "LT" => array(ArgType::Var, ArgType::Symbol, ArgType::Symbol),
    "GT" => array(ArgType::Var, ArgType::Symbol, ArgType::Symbol),
    "EQ" => array(ArgType::Var, ArgType::Symbol, ArgType::Symbol),
    "AND" => array(ArgType::Var, ArgType::Bool | ArgType::Var, ArgType::Bool | ArgType::Var),
    "OR" => array(ArgType::Var, ArgType::Bool | ArgType::Var, ArgType::Bool | ArgType::Var),
    "NOT" => array(ArgType::Var, ArgType::Bool | ArgType::Var),
    "INT2CHAR" => array(ArgType::Var, ArgType::Int | ArgType::Var),
    "STRI2INT" => array(ArgType::Var, ArgType::String | ArgType::Var, ArgType::Int | ArgType::Var),
    "READ" => array(ArgType::Var, ArgType::Type),
    "WRITE" => array(ArgType::Symbol),
    "CONCAT" => array(ArgType::Var, ArgType::String | ArgType::Var, ArgType::String | ArgType::Var),
    "STRLEN" => array(ArgType::Var, ArgType::String | ArgType::Var),
    "GETCHAR" => array(ArgType::Var, ArgType::String | ArgType::Var, ArgType::Int | ArgType::Var),
    "SETCHAR" => array(ArgType::Var, ArgType::Int | ArgType::Var, ArgType::String | ArgType::Var),
    "TYPE" => array(ArgType::Var, ArgType::Symbol),
    "LABEL" => array(ArgType::Label),
    "JUMP" => array(ArgType::Label),
    "JUMPIFEQ" => array(ArgType::Label, ArgType::Symbol, ArgType::Symbol),
    "JUMPIFNEQ" => array(ArgType::Label, ArgType::Symbol, ArgType::Symbol),
    "EXIT" => array(ArgType::Symbol),
    "DPRINT" => array(ArgType::Symbol),
    "BREAK" => array(),
    "ADDS" => array(),
    "CLEARS" => array(),
    "SUBS" => array(),
    "MULS" => array(),
    "IDIVS" => array(),
    "DIVS" => array(),
    "LTS" => array(),
    "GTS" => array(),
    "EQS" => array(),
    "ORS" => array(),
    "ANDS" => array(),
    "NOTS" => array(),
    "INT2CHARS" => array(),
    "STRI2INTS" => array(),
    "JUMPIFEQS" => array(ArgType::Label),
    "JUMPIFNEQS" => array(ArgType::Label),
    "INT2FLOAT" => array(ArgType::Var, ArgType::Int),
    "FLOAT2INT" => array(ArgType::Var, ArgType::Int),
    "INT2FLOATS" => array(),
    "FLOAT2INTS" => array(),
);

function check_type($instr, $arg, $arg_num)
{
    global $arg_types, $opcodes;
    if (($arg_types[$arg] & $opcodes[$instr][$arg_num]) == 0) {
        err("$instr: wrong operand type");
        return 23;
    }
    return 0;
}


// wrapper for error printing.
// Should make it easy to redirect, change format or disable errors
function err($msg)
{
    fwrite(STDERR, $msg . "\n");
}

// returns:
// - 0 if program should continue (no argument was passed)
// - 1 if help was printed, program should exit
// - 10 if invalid parameters were passed, program should exit
function check_args($argv)
{
    //no arguments
    if (count($argv) == 1)
        return 0;
    // assignment says help cannot be combined with anything else
    // but help is only valid argument -> 2 or more arguments are not a valid combination
    if (count($argv) > 2)
        return 10;

    if ($argv[1] == "--help") {


        print("Použití: php {$argv[0]} [--help]

STDIN - vstupní IPPCode
STDOUT - výstupní xml

Návratové hodnoty:
10 - Neplatné parametry
21 - Chybějící hlavička
22 - Neznámý nebo neplatný operační kód na STDIN
23 - Jiná chyba na STDIN
99 - Interní chyba
");
        return 1;
    } else {
        err("Invalid combination of arguments");
        return 10;
    }
    return 0;
}

// Class handling creation and inserting to xml using xmlwriter
class parser
{
    private $xml, $argc, $instr_num;
    public function __construct()
    {
        //initialize xmlwriter
        $this->xml = xmlwriter_open_memory();
        xmlwriter_set_indent($this->xml, 1);

        xmlwriter_set_indent_string($this->xml, ' ');
        xmlwriter_start_document($this->xml, '1.0', 'UTF-8');
        //create root element with language attribute

        $this->start_ele('program');
        $this->add_attr('language', 'IPPcode23');
        // initialize argument and instruction counters
        $this->argc = 0;
        $this->instr_num = 1;
    }

    // write xml to stdout
    public function write()
    {
        // end program element, not needed, wmlwrite would close all open elements
        $this->end_ele();
        xmlwriter_end_document($this->xml);
        echo xmlwriter_output_memory($this->xml);
    }

    // add instruction with args
    public function add_instr($opcode, $args)
    {
        $this->start_ele('instruction');
        $this->add_attr("order", $this->instr_num);
        $this->instr_num += 1; // increment instruction counter

        $this->add_attr("opcode", $opcode);

        $this->argc = 0; //reset argument ocunter
        // add non-empty arguments to xml
        foreach ($args as $arg) {
            if ($arg != "") {
                $ret = $this->add_arg($opcode, $args);
                if ($ret != 0) return $ret;
            }
        }
        $this->end_ele();
        return 0;
    }

    // check if label or function name contains valid characters
    private function check_label($lhs, $rhs)
    {
        if (
            $lhs != "" &&
            $lhs != "GF" &&
            $lhs != "LF" &&
            $lhs != "TF"
        ) {
            return 23;
        }

        $pattern = '/^[a-zA-Z-_$%&*!?][a-zA-Z0-9-_$%&*!?]*$/';
        if (!preg_match($pattern, $rhs)) { // check the right side with regex
            err("Invalid character in identifier $rhs");
            return 23;
        }
    }
    // checks if $str is valid ippcode string
    // returns 23 on failure and prints error
    // returns 0 on suecess
    private function check_str($str)
    {
        //$pattern = '/^(\\\d{3}|[^#\\\s])*$/';
        //matches
        $pattern = '/'
                 .'^'//beginning of string followed by
                 .'('// either
                 .'\\\d{3}'// symbol '\' followed by three digits (0-9 three times)
                 .'|'// or
                 .'[^#\\\s]'// anything but '#', '\' or whitespace
                 .')*'//zero or more times
                 .'$/';//followed by end of the string
        // but because in php's single quoted string \\ is valid escape sequence
        // \\ must be replaces with \\\\, this way php escapes quadruple backslash and
        // replaces is with double and regex engine receives double backslash.
        $pattern = '/^(\\\\\d{3}|[^#\\\\\s])*$/';
        if (!preg_match($pattern, $str)) { // check the right side with regex
            err("Invalid string \"$str\"");
            return 23;
        }
    }

    private function add_arg($instr, $args)
    {
        $arg = $args[$this->argc];
        $separated = explode('@', $arg, 2);
        $this->start_ele("arg" . ((string)$this->argc + 1));
        switch ($separated[0]) {
            case "nil":
                //check correct format of nil
                if ($separated[1] != "nil") {
                    err("expected contant of type nil to have value nil, not \"$separated[1]\"(aka nil@nil, not nil@$separated[1])");
                    return 23;
                }
            case "bool":
            case "int":
            case "float":
                //check for empty contants
                if (count($separated) > 1 && $separated[1] == "") {
                    err("Empty number constant on line $this->instr_num");
                    return 23;
                }
            case "string":
                if (count($separated) > 1 && $separated[0] == "string" && $this->check_str($separated[1])) return 23;
                if (count($separated) == 1) {
                    // this should only be valid for second argument of READ
                    if (check_type($instr, "type", $this->argc)) return 23;
                    $this->add_attr("type", "type");
                } else { // constants
                    if (check_type($instr, $separated[0], $this->argc)) return 23;
                    $this->add_attr("type", $separated[0]);
                }
                break;
            case "GF":
            case "TF":
            case "LF":
                //variables
                if (check_type($instr, "var", $this->argc)) return 23;
                $this->add_attr("type", "var");
                $ret = $this->check_label($separated[0], $separated[1]);
                if ($ret != 0) {
                    return $ret;
                }

                // var name was separated from frame, we need to reconnect it
                $separated[1] = $separated[0] . '@' . $separated[1];
                break;
            default:
                if (count($separated) == 1) {
                    if (check_type($instr, "label", $this->argc)) return 23;
                    $ret = $this->check_label("", $separated[0]);
                    if ($ret != 0) {
                        return $ret;
                    }
                    $this->add_attr("type", "label");
                } else {
                    // argument is in format xxx@yyy, but xxx is not recognised
                    err("Unknown type {$separated[0]} in {$arg}\n");
                    return 23;
                }
                break;
        }
        xmlwriter_text($this->xml, end($separated));
        $this->end_ele();
        $this->argc += 1;
        return 0;
    }
    private function add_attr($name, $value)
    {
        xmlwriter_start_attribute($this->xml, $name);
        xmlwriter_text($this->xml, $value);
        xmlwriter_end_attribute($this->xml);
    }
    private function start_ele($name)
    {
        xmlwriter_start_element($this->xml, $name);
    }
    private function end_ele()
    {
        xmlwriter_end_element($this->xml);
    }
    public function get_inst_num()
    {
        return $this->instr_num;
    }
}



// Processes single line of IPPCode passed in $line.
// Requires initialized xml class in $xml
function read_line($line, $xml)
{
    // 1 read_line call == 1 line of source code => $line_num is line counter
    // for error messages
    static $line_num = 1;
    $line_num += 1;
    global $opcodes;
    // regex to remove comments
    // Check patter - to check rough format of input
    $check_pattern = '/^(([^ #]+ *){0,4}) *(#.*)?$\n?/i';
    // select pattern - to remove whitespaces, comments and so
    $select_pattern = '/^([^ #]*) *([^ #]*) *([^ #]*) *([^ #]*) *.*$/i';
    $replacement = '\1 \2 \3 \4';
    $count = 0; //< Number how many the regex patter matched, 0 -> error
    $line = preg_replace($select_pattern, $replacement, $line, -1, $count);
    // This should fail only when line contains more than 4 "things" separated
    // by whitespace (not counting comments)
    if (!preg_match($check_pattern, $line)) {
        err("Invalid number of arguments on line {$line_num}");
        return 23;
    }
    // split string by whitespace
    $separated = explode(' ', trim($line));
    //check for empty lines
    if (
        count($separated) == 0 ||
        (count($separated) == 1 && $separated[0] == '')
    ) {
        return 0;
    }
    // opcodes are not case sensitive - convert everything to uppercase
    $separated[0] = strtoupper($separated[0]);
    // check for valid instruction
    if (!isset($opcodes[$separated[0]])) {
        err("Invalid opcode: \"{$separated[0]}\" on line: {$line_num}\n");
        return 22;
    }
    // check number of arguments
    if (count($separated) - 1 != count($opcodes[$separated[0]])) {
        echo "separated: ", count($separated), "\n";
        err("Invalid number of arguments on line: {$line_num}\n");
        return 23;
    }
    return $xml->add_instr($separated[0], array_slice($separated, 1));
}

// Read $file until eof
// returns expected exit code on error
function read_file($xml, $file)
{
    while (($line = fgets($file)) !== false) {
        $ret = read_line($line, $xml);
        if ($ret != 0) return $ret;
    }
    return 0;
}

$ret = check_args($argv);
if ($ret != 0){
    if($ret == 1)
        exit(0);
    exit($ret);
}

$xml = new parser();

// matches for ".IPPCode23" and comment after that
$pattern = '/^ *\.IPPCode23 *(#.*)?$/i';
//matches comment or empty line
$alt_pattern = '/^ *(#.*)?$/';
$header_valid = false;
while ($header_valid == false) {
    if (($line = fgets(STDIN)) !== false) { // attempt to read a line from input
        // check if line contains valid header
        if (!preg_match($pattern, $line)) {
            // line did not contain valid header, check if it's empty or comment
            if (!preg_match($alt_pattern, $line)) {
                // line contains neither header nor comment - error
                err("invalid header, expected \".IPPCode23\"\n");
                exit(21);
            }
        } else { // valid ippcode header found
            $header_valid = true;
        }
    } else { // failed to read line from input
        err("missing header, expected \".IPPCode23\"\n");
        exit(21);
    }
}

$ret = read_file($xml, STDIN);
if ($ret != 0) exit($ret);

$xml->write();
