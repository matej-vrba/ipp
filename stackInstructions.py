from baseInstruction import instruction, same_type_or_throw, type_any
import abc

from argument import ArgType
from util import BAD_TYPE, dp, Err, BAD_VAL, STRING_ERR



class PUSHS(instruction):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, type_any, 1)

    def exec(self, context):
        val = context.get_value(self.args[0], [ArgType.Any])
        type = context.get_type(self.args[0])
        context.data_stack_push((val, type))



class POPS(instruction):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, type_any, 1)


    def exec(self, context):
        (val, type) = context.data_stack_pop()

        context.set(self.args[0], val, type)


class CLEARS(instruction):
    arg_n = 0

    def check_arg_types(self):
        return

    def exec(self, context):
        context.data_stack_clear()


class StackArithmeticInstr(instruction):
    """Base class for arithmetic stack instructions.

    Check correct types, deriving classes only implement method run with bare
    minimum
    """
    arg_n = 0

    def check_arg_types(self):
        return

    @abc.abstractmethod
    def run(self, first, second, type):
        raise NotImplementedError()

    def exec(self, context):
        (second, s_type) = context.data_stack_pop()
        (first, f_type) = context.data_stack_pop()
        if f_type != s_type or f_type not in [ArgType.Int, ArgType.Float]:
            raise Err(err_msg="Cannot call {} with types {} and {}"
                      .format(type(self).__name__, f_type, s_type),
                      exit_code=BAD_TYPE)
        result = self.run(first, second, f_type)
        context.data_stack_push((result, f_type))


class ADDS(StackArithmeticInstr):
    def run(self, first, second, type):
        return first + second


class SUBS(StackArithmeticInstr):
    def run(self, first, second, type):
        return first - second


class MULS(StackArithmeticInstr):
    def run(self, first, second, type):
        return first * second


class IDIVS(StackArithmeticInstr):
    def run(self, first, second, type):
        if type != ArgType.Int:
            raise Err(err_msg="IDIVS: Invalid argument type {} (must be int)"
                      .format(type), exit_code=BAD_TYPE)

        if(second == 0):
            raise Err(err_msg="IDIVS: Division by 0", exit_code=BAD_VAL)
        return int(first // second)

class DIVS(StackArithmeticInstr):
    def run(self, first, second, type):
        if type != ArgType.Float:
            raise Err(err_msg="IDIVS: Invalid argument type {} (must be int)"
                      .format(type), exit_code=BAD_TYPE)
        if(second == 0):
            raise Err(err_msg="IDIVS: Division by 0", exit_code=BAD_VAL)
        return first / second

class StackCMPInstr(instruction):
    """Same as stack arithmetic instructions, base class does most of the
    checking, deriving classes implement `run` doing bare minimum"""
    arg_n = 0

    def check_arg_types(self):
        return

    @abc.abstractmethod
    def run(self, first, second, type):
        raise NotImplementedError()

    def exec(self, context):
        (second, s_type) = context.data_stack_pop()
        (first, f_type) = context.data_stack_pop()
        if f_type != s_type or (f_type not in [ArgType.Int, ArgType.Bool, ArgType.String, ArgType.Float]):
            raise Err(err_msg="Cannot call {} with types {} and {}"
                      .format(type(self).__name__, f_type, s_type),
                      exit_code=BAD_TYPE)
        result = self.run(first, second, f_type)
        context.data_stack_push((result, ArgType.Bool))

class LTS(StackCMPInstr):
    def run(self, first, second, type):
        return first < second

class GTS(StackCMPInstr):
    def run(self, first, second, type):
        return first > second


class EQS(StackCMPInstr):
    def run(self, first, second, type):
        return first == second

class StackLogInstr(instruction):
    arg_n = 0

    def check_arg_types(self):
        return

    @abc.abstractmethod
    def run(self, first, second):
        raise NotImplementedError()

    def exec(self, context):
        if type(self) != NOTS:
            (second, s_type) = context.data_stack_pop()
        else:
            second = None
            s_type = ArgType.Bool

        (first, f_type) = context.data_stack_pop()
        if f_type != s_type or f_type != ArgType.Bool:
            raise Err(err_msg="Cannot call {} with types {} and {}"
                      .format(type(self).__name__, f_type, s_type),
                      exit_code=BAD_TYPE)
        result = self.run(first, second)
        context.data_stack_push((result, ArgType.Bool))

class ORS(StackLogInstr):
    def run(self, first, second):
        return first or second

class ANDS(StackLogInstr):
    def run(self, first, second):
        return first and second

class NOTS(StackLogInstr):
    def run(self, first, second):
        return not first

class INT2CHARS(instruction):
    arg_n = 0

    def check_arg_types(self):
        return

    def exec(self, context):
        (val, val_type) = context.data_stack_pop()

        if val_type != ArgType.Int:
            raise Err(err_msg="Cannot call {} with type {}"
                      .format(type(self).__name__, val_type),
                      exit_code=BAD_TYPE)

        try:
            converted = chr(val)
        except ValueError:
            raise Err(STRING_ERR, "Failed to convert {} to unicode".format(val))

        context.data_stack_push((converted, ArgType.String))

class INT2FLOATS(instruction):
    arg_n = 0

    def check_arg_types(self):
        return

    def exec(self, context):
        (val, val_type) = context.data_stack_pop()

        if val_type != ArgType.Int:
            raise Err(err_msg="Cannot call {} with type {}"
                      .format(type(self).__name__, val_type),
                      exit_code=BAD_TYPE)

        try:
            converted = float(val)
        except ValueError:
            raise Err(STRING_ERR, "Failed to convert {} to float".format(val))

        context.data_stack_push((converted, ArgType.Float))


class FLOAT2INTS(instruction):
    arg_n = 0

    def check_arg_types(self):
        return

    def exec(self, context):
        (val, val_type) = context.data_stack_pop()

        if val_type != ArgType.Float:
            raise Err(err_msg="Cannot call {} with type {}"
                      .format(type(self).__name__, val_type),
                      exit_code=BAD_TYPE)

        try:
            converted = int(val)
        except ValueError:
            raise Err(STRING_ERR, "Failed to convert {} to int".format(val))

        context.data_stack_push((converted, ArgType.Int))


class STRI2INTS(instruction):
    arg_n = 0

    def check_arg_types(self):
        return

    def exec(self, context):
        (second, s_type) = context.data_stack_pop()
        (first, f_type) = context.data_stack_pop()

        if f_type != ArgType.String:
            raise Err(err_msg="Cannot call {} on {} (must be a string)"
                      .format(type(self).__name__, f_type),
                      exit_code=BAD_TYPE)

        if s_type != ArgType.Int:
            raise Err(err_msg="{} Cannot index with type {} (must be int)"
                      .format(type(self).__name__, s_type),
                      exit_code=BAD_TYPE)

        if second >= len(first):
            raise Err(STRING_ERR, "STRI2INTS: Index {} out of bounds of string \"{}\"".format(second, first))

        converted = ord(first[second])

        context.data_stack_push((converted, ArgType.Int))


class StackJMPInstr(instruction):
    arg_n = 1

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Label], 1)

    @abc.abstractmethod
    def run(self, first, second):
        raise NotImplementedError()

    def exec(self, context):
        (second, s_type) = context.data_stack_pop()
        (first, f_type) = context.data_stack_pop()
        if f_type != s_type or (f_type not in [ArgType.Int, ArgType.Bool, ArgType.String, ArgType.Float]):
            raise Err(err_msg="Cannot call {} with types {} and {}"
                      .format(type(self).__name__, f_type, s_type),
                      exit_code=BAD_TYPE)

        # JMPIFEQS and JMPIFNEQS return from run methow true if instruction
        # should follor jump, false otherwise
        if self.run(first, second):
           return self.args[0].val


class JUMPIFEQS(StackJMPInstr):
    def run(self, first, second):
        return first == second

class JUMPIFNEQS(StackJMPInstr):
    def run(self, first, second):
        return first != second
