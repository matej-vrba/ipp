from baseInstruction import instruction, same_type_or_throw, type_any

from argument import ArgType
from util import dp, Err, STRING_ERR

class CONCAT(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.String], 2
        )
        same_type_or_throw(
            self.args[2].type, [ArgType.Var, ArgType.String], 3
        )

    def exec(self, context):
        # if operator is String, use string, if it's uninitialized use empty
        # string, report error otherwise

        left = context.get_value(self.args[1], [ArgType.String])
        right = context.get_value(self.args[2], [ArgType.String])
        # result will be string
        new_type = ArgType.String
        new_val = left + right
        context.set(self.args[0], new_val, new_type)



class STRLEN(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.String], 2
        )

    def exec(self, context):

        to_measure = context.get_value(self.args[1], [ArgType.String])

        context.set(self.args[0], len(to_measure), ArgType.Int)



class GETCHAR(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(self.args[1].type, [ArgType.Var, ArgType.String], 2)
        same_type_or_throw(self.args[2].type, [ArgType.Var, ArgType.Int], 3)

    def exec(self, context):
        # if operator is String, use string, if it's uninitialized use empty
        # string, report error otherwise

        dst = context.get_value(self.args[0], [ArgType.String])
        src = context.get_value(self.args[1], [ArgType.String])
        src_pos = context.get_value(self.args[2], [ArgType.Int])

        if src_pos >= len(src):
            raise Err(STRING_ERR, "GETCHAR: index {} out of bounds of source string \"{}\"".format(src_pos, src))

        dst = src[src_pos]

        context.set(self.args[0], dst, ArgType.String)



class SETCHAR(instruction):
    arg_n = 3

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(
            self.args[1].type, [ArgType.Var, ArgType.Int], 2
        )
        same_type_or_throw(
            self.args[2].type, [ArgType.Var, ArgType.String], 3
        )

    def exec(self, context):
        # if operator is String, use string, if it's uninitialized use empty
        # string, report error otherwise

        dst = context.get_value(self.args[0], [ArgType.String])
        pos = context.get_value(self.args[1], [ArgType.Int])
        src = context.get_value(self.args[2], [ArgType.String])

        if pos >= len(dst):
            raise Err(STRING_ERR, "SETCHAR: index {} is past end of destination string \"{}\"".format(pos, dst))

        if pos < 0:
            raise Err(STRING_ERR, "SETCHAR: Invalid index {}".format(pos))

        if len(src) == 0:
            raise Err(STRING_ERR, "SETCHAR: Source string cannot be empty")

        dst = dst[:pos] + src[0] + dst[pos+1:]

        context.set(self.args[0], dst, ArgType.String)




class TYPE(instruction):
    arg_n = 2

    def check_arg_types(self):
        same_type_or_throw(self.args[0].type, [ArgType.Var], 1)
        same_type_or_throw(self.args[1].type, type_any, 2)

    def exec(self, context):
        type = context.get_type(self.args[1])

        tab = {
            ArgType.String: "string" ,
            ArgType.Bool: "bool" ,
            ArgType.Int: "int" ,
            ArgType.Nil: "nil" ,
            ArgType.Type: "type" ,
            ArgType.Float: "float" ,
        }

        if tab.get(type) is None:
            context.set(self.args[0], "", ArgType.String)
        else:
            context.set(self.args[0], tab[type], ArgType.String)

        return

        #context.set(self.args[0], converted, ArgType.String)
