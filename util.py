#!/usr/bin/env python3
# debug
import pprint
import traceback
import sys

MALFORMED_XML = 32
BAD_XML_CONTENT = 32
BAD_SOURCE = 52
BAD_TYPE = 53
NO_FRAME = 55
NO_VAL = 56
BAD_VAL = 57
ERR_INTERNAL = 99
STRING_ERR = 58

# helper function when using pdb
def dp(val):
    pprint.pprint(vars(val))


def eprint(msg, *args):
    """Prints to stderr"""
    print(msg, file=sys.stderr, *args)


# used to mark parts of code not yet implemented
def todo():
    eprint("Not implemented")
    breakpoint()
    raise Err(99)


# Exception used fo bulk of error handling
class Err(Exception):
    def __init__(self, exit_code, err_msg):
        super().__init__(err_msg)

        self.exit_code = exit_code
